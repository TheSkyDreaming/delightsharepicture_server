package cn.delightsharepicture.exceptionhandle.exception;


import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MyException extends RuntimeException{
    private ExceptionEnum exceptionEnum;
}
