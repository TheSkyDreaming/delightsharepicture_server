package cn.delightsharepicture.exceptionhandle.vo;

import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import lombok.Data;

@Data
public class ExceptionResult {
    private int statusCode;
    private String message;
    private Long timeStamp;
    public ExceptionResult(ExceptionEnum eenum){
        this.statusCode=eenum.getCode();
        this.message=eenum.getMsg();
        this.timeStamp=System.currentTimeMillis();
    }
}
