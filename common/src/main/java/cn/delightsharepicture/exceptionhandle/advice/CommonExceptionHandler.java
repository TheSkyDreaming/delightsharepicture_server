package cn.delightsharepicture.exceptionhandle.advice;

import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import cn.delightsharepicture.exceptionhandle.exception.MyException;
import cn.delightsharepicture.exceptionhandle.vo.ExceptionResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CommonExceptionHandler {
//    自定义异常
    @ExceptionHandler(MyException.class)
    public ResponseEntity<ExceptionResult> handlerException(MyException e){
        return ResponseEntity.ok(new ExceptionResult(e.getExceptionEnum()));
    }
//    其他的异常
//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<ExceptionResult> handlerOtherException(Exception e){
//        e.printStackTrace();
//        return ResponseEntity.ok(new ExceptionResult(ExceptionEnum.OTHER_EXCEP));
//    }
}
