package cn.delightsharepicture.exceptionhandle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum ExceptionEnum {
    OTHER_EXCEP(999,"其他异常发生了"),
    PARAM_NOT_INTEGRITY(1000,"参数不完整"),
    NOT_LOGIN(1001,"未登录"),
    PASS_ERROR(1002,"密码错误"),
    USER_NOT_EXIST(1003,"用户不存在"),
    USER_EXISTED(1004,"用户已存在"),
    OPERATION_FAILED(1004,"操作失败"),
    CHECKCODE_ERROR(1005,"验证码错误"),
    RES_NAME_CANNOT_BE_NULL(1006,"资源名称不能为空"),
    RES_IS_NULL(1007,"没有查找到资源"),
    FILE_UPLOAD_FAILED(1008,"文件上传失败"),
    PARAM_NOT_DIGIT(1009,"请传递数字类型的参数"),
    JSON_CONVERT_ERROR(1010,"json解析错误"),
    INVOKE_FAILED(1011,"服务调用失败"),
    ACCESS_DENIED(1012,"权限不足")
    ;
    private int code;
    private String msg;
}
