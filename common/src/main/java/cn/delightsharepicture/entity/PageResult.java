package cn.delightsharepicture.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageResult {
    private int currPage;
    private int pageSize;
    private int totalPages;
    private long totalSize;
    private List<?> data;
}
