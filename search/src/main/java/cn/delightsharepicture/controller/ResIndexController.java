package cn.delightsharepicture.controller;

import cn.delightsharepicture.entity.ResultEntity;
import cn.delightsharepicture.service.ResIndexService;
import com.netflix.discovery.converters.Auto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/index")
@Api(description = "索引查询模块")
public class ResIndexController {

    @Autowired
    private ResIndexService resIndexService;

    @ApiOperation("分页查询索引")
    @GetMapping("/{key}/{orderKey}/{orderStyle}/{page}/{size}")
    public ResponseEntity<ResultEntity> findByKey(@PathVariable("key") String key,@PathVariable("orderKey") String orderKey,@PathVariable("orderStyle") String orderStyle,@PathVariable("page") int page,@PathVariable("size") int size){
        return ResponseEntity.ok(new ResultEntity(200,resIndexService.findByKey(key,orderKey,orderStyle,page,size)));
    }
}
