package cn.delightsharepicture.service.impl;

import cn.delightsharepicture.dao.ResIndexDao;
import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import cn.delightsharepicture.exceptionhandle.exception.MyException;
import cn.delightsharepicture.pojo.PageResult;
import cn.delightsharepicture.pojo.ResIndex;
import cn.delightsharepicture.service.ResIndexService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ResIndexServiceImpl implements ResIndexService {

    @Autowired
    private ResIndexDao resIndexDao;

    @Override
    public PageResult findByKey(String key, String orderKey, String orderStyle, int page, int size) {
        if (StringUtils.isBlank(key) || StringUtils.isBlank(orderKey) || StringUtils.isBlank(orderStyle)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
//        分页参数合法化
        page = page < 1 ? 1 : page;
        size = size < 1 ? 1 : size;
        Sort sort;
        String orderKeyDB = getOrderKeyDB(orderKey);
        String orderStyleDB = getOrderStyleDB(orderStyle);
        if ("DESC".equals(orderStyleDB)) {
            sort = new Sort(Sort.Direction.DESC, orderKeyDB);
        } else {
            sort = new Sort(Sort.Direction.ASC, orderKeyDB);
        }
        Pageable pageable = PageRequest.of(page - 1, size, sort);
        Page<ResIndex> pageRes = resIndexDao.findByResnameOrNicknameLike(key, key, pageable);
//        将所搜到的数据对应资源ID返回给resource服务。
        List<Integer> ids = new ArrayList<>();
        List<ResIndex> content = pageRes.getContent();
        Iterator<ResIndex> iterator = content.iterator();
        while (iterator.hasNext()) {
            ResIndex tem = iterator.next();
            ids.add(tem.getRid());
        }
        PageResult result = new PageResult();
        result.setCurrPage(pageRes.getNumber()+1);
        result.setPageSize(pageRes.getSize());
        result.setTotalPages(pageRes.getTotalPages());
        result.setTotalSize(pageRes.getTotalElements());
        result.setData(ids);
        return result;
    }

    public String getOrderKeyDB(String orderKey) {
        String orderKeyDB = "uptime";
        if (orderKey.equals("down")) {
            orderKeyDB = "downloaded";
        }
        return orderKeyDB;
    }

    public String getOrderStyleDB(String orderStyle) {
        String orderStyleDB = "DESC";
        if (orderStyle.equals("a")) {
            orderStyleDB = "ASC";
        }
        return orderStyleDB;
    }
}
