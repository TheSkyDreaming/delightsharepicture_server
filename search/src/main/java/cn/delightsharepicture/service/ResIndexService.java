package cn.delightsharepicture.service;

import cn.delightsharepicture.pojo.PageResult;


public interface ResIndexService {
    PageResult findByKey(String key, String orderKey, String orderStyle, int page, int size);
}
