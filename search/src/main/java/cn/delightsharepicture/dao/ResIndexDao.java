package cn.delightsharepicture.dao;

import cn.delightsharepicture.pojo.ResIndex;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ResIndexDao extends ElasticsearchRepository<ResIndex,Integer> {
    Page<ResIndex> findByResnameOrNicknameLike(String resname, String nickname, Pageable pageable);
}
