package cn.delightsharepicture.service;

import cn.delightsharepicture.pojo.AddResParam;
import cn.delightsharepicture.entity.PageResult;

public interface ResourceService {
    PageResult getAllHomeResByPage(String orderKey, String orderStyle, String currPage, String pageSize);

    PageResult getClassHomeResByPage(String classify, String orderKey, String orderStyle, String currPage, String pageSize);

    PageResult search(String keyWord, String orderKey, String orderStyle, String currPage, String pageSize);

    String add(AddResParam param);

    String delete(String rid);

    String download(String fileType,String rid);

    PageResult getPersBackResByPage(String uid, String orderKey, String orderStyle, String currPage, String pageSize);

    PageResult getAllBackResByPage(String orderKey, String orderStyle, String currPage, String pageSize);
}
