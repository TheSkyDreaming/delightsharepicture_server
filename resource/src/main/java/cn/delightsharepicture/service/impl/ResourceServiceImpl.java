package cn.delightsharepicture.service.impl;

import cn.delightsharepicture.dao.ClassifyDao;
import cn.delightsharepicture.dao.PathDao;
import cn.delightsharepicture.dao.ResourceDao;
import cn.delightsharepicture.entity.PageResult;
import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import cn.delightsharepicture.exceptionhandle.exception.MyException;
import cn.delightsharepicture.feignClients.SearchClient;
import cn.delightsharepicture.feignClients.UserClient;
import cn.delightsharepicture.pojo.*;
import cn.delightsharepicture.service.ResourceService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class ResourceServiceImpl implements ResourceService {
    @Autowired
    private ResourceDao resDao;
    @Autowired
    private PathDao pathDao;
    @Autowired
    ClassifyDao claDao;
    @Autowired
    private ObjectMapper objMap;
    @Autowired
    private UserClient userClient;
    @Autowired
    SearchClient searchClient;
    @Autowired
    HttpServletRequest request;
    @Autowired
    Environment env;

    /*
        经过测试feignclient可以再service层使用，但是不知道是否也可以在
        其他已经注入spring容器的其他组件中使用。*/

    @Override
    public PageResult getAllHomeResByPage(String orderKey, String orderStyle, String currPage, String pageSize) {
        if (!strNoNull(orderKey, orderStyle, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!strIsNumeric(currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        Page page = PageHelper.startPage(Integer.parseInt(currPage), Integer.parseInt(pageSize), getOrderKeyDB(orderKey) + " " + getOrderStyleDB(orderStyle));
        List<Resource> ress = resDao.selectAll();
        if (ress == null || ress.size() == 0) {
            throw new MyException(ExceptionEnum.RES_IS_NULL);
        }
        PageResult result = new PageResult();
        result.setCurrPage(page.getPageNum());
        result.setPageSize(page.getPageSize());
        result.setTotalPages(page.getPages());
        result.setTotalSize(page.getTotal());
        return combinePageData(result, ress);
    }

    @Override
    public PageResult getClassHomeResByPage(String classify, String orderKey, String orderStyle, String currPage, String pageSize) {
        if (!strNoNull(classify, orderKey, orderStyle, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!strIsNumeric(classify, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        int digitClass = Integer.parseInt(classify);
        Page page = PageHelper.startPage(Integer.parseInt(currPage), Integer.parseInt(pageSize), getOrderKeyDB(orderKey) + " " + getOrderStyleDB(orderStyle));
        List<Resource> ress = resDao.pageOrderClassifyFind(digitClass);
        PageResult result = new PageResult();
        result.setCurrPage(page.getPageNum());
        result.setPageSize(page.getPageSize());
        result.setTotalPages(page.getPages());
        result.setTotalSize(page.getTotal());
        return combinePageData(result, ress);
    }

    @Override
    public PageResult search(String keyWord, String orderKey, String orderStyle, String currPage, String pageSize) {
        if (!strNoNull(keyWord, orderKey, orderStyle, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!strIsNumeric(currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        int digitCurrPage = Integer.parseInt(currPage);
        int digitPageSize = Integer.parseInt(pageSize);
        String searchResult = searchClient.search(keyWord, orderKey, orderStyle, digitCurrPage, digitPageSize);
        SearchStorage searchStorage = null;
        try {
            searchStorage = objMap.readValue(searchResult, SearchStorage.class);
        } catch (IOException e) {
            throw new MyException(ExceptionEnum.JSON_CONVERT_ERROR);
        }
        List<Integer> rids = (List<Integer>) searchStorage.getResult().getData();
        List<Resource> ress = resDao.containsIdFind(rids, getOrderKeyDB(orderKey), getOrderStyleDB(orderStyle));
        if (null == ress) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        PageResult result = searchStorage.getResult();
        return combinePageData(result, ress);
    }

    @Override
    public String add(AddResParam param) {
        boolean infoIntegrity = false;
        String resname = "";
        Integer uid = 0;
        Integer cid = 0;
        MultipartFile picFile = null;
        MultipartFile psdFile = null;
        if (param != null) {
            resname = param.getResname();
            uid = param.getUid();
            cid = param.getCid();
            picFile = param.getPicfile();
            psdFile = param.getPsdfile();
            if (StringUtils.isNotBlank(resname) && uid > 0 && cid > 0 && null != picFile && null != psdFile) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if(null==request.getAttribute("token")){
            throw new MyException(ExceptionEnum.ACCESS_DENIED);
        }
        String picPath = fileUpload(picFile);
        String psdPath = fileUpload(psdFile);
        Path path = new Path();
        path.setPicpath(picPath);
        path.setPsdpath(psdPath);
        int pathAdd = pathDao.insert(path);
        if (pathAdd != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        Resource res = new Resource();
        res.setResname(resname);
        res.setUid(uid);
        res.setCid(cid);
        res.setPid(path.getPid());
        res.setUptime(new Date());
        int resourceAdd = resDao.insert(res);
        if (resourceAdd != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        String invokeRes = userClient.addUploadNum(uid.toString());
        CommonStorage commonStorage = null;
        try {
            commonStorage = objMap.readValue(invokeRes, CommonStorage.class);
        } catch (IOException e) {
            throw new MyException(ExceptionEnum.JSON_CONVERT_ERROR);
        }
        if (commonStorage == null || commonStorage.getStatusCode() != 200) {
            throw new MyException(ExceptionEnum.INVOKE_FAILED);
        }
        return "上传资源成功";
    }

    @Override
//    @Transactional
    public String delete(String rid) {
        if (StringUtils.isBlank(rid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!StringUtils.isNumeric(rid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        if(null==request.getAttribute("token")){
            throw new MyException(ExceptionEnum.ACCESS_DENIED);
        }
        Resource temRes = resDao.selectByPrimaryKey(Integer.parseInt(rid));
        if (null == temRes) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        int delNum = pathDao.deleteByPrimaryKey(temRes.getPid());
        if (delNum > 0) {
            return "删除成功";
        } else {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
    }

    @Override
//    @Transactional
//    这里不能使用@Transactional，否则会导致feignClient调用失败，原因未知，或许可以考虑分布式事务解决方案
    public String download(String fileType, String rid) {
        if (!strNoNull(fileType, rid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!StringUtils.isNumeric(rid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        String retResult = "后台更新成功";
//        判断是否是下载psd文件，如果是则请求返回psd路径
        if (fileType.equals("psd")) {
            String psdPath = resDao.findPsdPath(Integer.parseInt(rid));
            if (StringUtils.isBlank(psdPath)) {
                throw new MyException(ExceptionEnum.OPERATION_FAILED);
            }
            retResult = psdPath;
        }
//        增加资源下载量
        int affectRow = resDao.addResDownQuantity(Integer.parseInt(rid));
        if (affectRow != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
//        增加资源所有者总下载量
        Resource resource = resDao.selectByPrimaryKey(Integer.parseInt(rid));
        if (null == resource) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        String invokeRes = userClient.addDownNum(resource.getUid().toString());
        CommonStorage commonStorage = null;
        try {
            commonStorage = objMap.readValue(invokeRes, CommonStorage.class);
        } catch (IOException e) {
            throw new MyException(ExceptionEnum.JSON_CONVERT_ERROR);
        }
        if (commonStorage == null || commonStorage.getStatusCode() != 200) {
            throw new MyException(ExceptionEnum.INVOKE_FAILED);
        }
//        如果是下载图片文件直接返回一个无意义的字符串。
        return retResult;
    }

    @Override
    public PageResult getPersBackResByPage(String uid, String orderKey, String orderStyle, String currPage, String pageSize) {
        if (!strNoNull(uid, orderKey, orderStyle, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!strIsNumeric(uid, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        int digitUid = Integer.parseInt(uid);
        Page page = PageHelper.startPage(Integer.parseInt(currPage), Integer.parseInt(pageSize), getOrderKeyDB(orderKey) + " " + getOrderStyleDB(orderStyle));
        List<Resource> ress = resDao.pageOrderPersonalFind(digitUid);
        //        分页主体数据（前端显示的内容）容器
        List<Map<String, Object>> allData = new ArrayList<>();
//        临时保存单条记录的容器
        Map<String, Object> tempMap;
        Iterator<Resource> ite = ress.iterator();
//        组装分页主体数据
        while (ite.hasNext()) {
            Resource res = ite.next();
            Classify temCla = claDao.selectByPrimaryKey(res.getCid());
            if (null == temCla) {
                throw new MyException(ExceptionEnum.OPERATION_FAILED);
            }
            tempMap = new HashMap<>();
            tempMap.put("resId", res.getRid());
            tempMap.put("name", res.getResname());
            tempMap.put("classify", temCla.getClaname());
            tempMap.put("uptime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(res.getUptime()));
            tempMap.put("downloaded", res.getDownloaded());
            allData.add(tempMap);
        }
        PageResult result = new PageResult();
        result.setCurrPage(page.getPageNum());
        result.setPageSize(page.getPageSize());
        result.setTotalPages(page.getPages());
        result.setTotalSize(page.getTotal());
        result.setData(allData);
        return result;
    }

    @Override
    public PageResult getAllBackResByPage(String orderKey, String orderStyle, String currPage, String pageSize) {
        if (!strNoNull(orderKey, orderStyle, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!strIsNumeric(currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        Page page = PageHelper.startPage(Integer.parseInt(currPage), Integer.parseInt(pageSize), getOrderKeyDB(orderKey) + " " + getOrderStyleDB(orderStyle));
        List<Resource> ress = resDao.selectAll();
        //        分页主体数据（前端显示的内容）容器
        List<Map<String, Object>> allData = new ArrayList<>();
//        临时保存单条记录的容器
        Map<String, Object> tempMap;
        Iterator<Resource> ite = ress.iterator();
//        组装分页主体数据
        while (ite.hasNext()) {
            Resource res = ite.next();
            Classify temCla = claDao.selectByPrimaryKey(res.getCid());
            if (null == temCla) {
                throw new MyException(ExceptionEnum.OPERATION_FAILED);
            }
            //            调用user服务获取昵称
            String nickResult = userClient.selectNickname(res.getUid().toString());
            CommonStorage commonStorage = null;
            try {
                commonStorage = objMap.readValue(nickResult, CommonStorage.class);
            } catch (IOException e) {
                throw new MyException(ExceptionEnum.JSON_CONVERT_ERROR);
            }
            if (commonStorage == null || commonStorage.getStatusCode() != 200) {
                throw new MyException(ExceptionEnum.INVOKE_FAILED);
            }
            tempMap = new HashMap<>();
            tempMap.put("resId", res.getRid());
            tempMap.put("name", res.getResname());
            tempMap.put("author", commonStorage.getResult());
            tempMap.put("classify", temCla.getClaname());
            tempMap.put("uptime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(res.getUptime()));
            tempMap.put("downloaded", res.getDownloaded());
            allData.add(tempMap);
        }
        PageResult result = new PageResult();
        result.setCurrPage(page.getPageNum());
        result.setPageSize(page.getPageSize());
        result.setTotalPages(page.getPages());
        result.setTotalSize(page.getTotal());
        result.setData(allData);
        return result;
    }

    private PageResult combinePageData(PageResult pageResult, List<Resource> ress) {
//        分页主体数据（前端显示的内容）容器
        List<Map<String, Object>> allData = new ArrayList<>();
//        临时保存单条记录的容器
        Map<String, Object> tempMap;
        Iterator<Resource> ite = ress.iterator();
//        组装分页主体数据
        while (ite.hasNext()) {
            Resource res = ite.next();
            Path tempPath = pathDao.selectByPrimaryKey(res.getPid());
            if (tempPath == null) {
                throw new MyException(ExceptionEnum.OPERATION_FAILED);
            }
//            调用user服务获取昵称
            String nickResult = userClient.selectNickname(res.getUid().toString());
            CommonStorage commonStorage = null;
            try {
                commonStorage = objMap.readValue(nickResult, CommonStorage.class);
            } catch (IOException e) {
                throw new MyException(ExceptionEnum.JSON_CONVERT_ERROR);
            }
            if (commonStorage == null || commonStorage.getStatusCode() != 200) {
                throw new MyException(ExceptionEnum.INVOKE_FAILED);
            }
            tempMap = new HashMap<>();
            tempMap.put("resId", res.getRid());
            tempMap.put("name", res.getResname());
            tempMap.put("src", tempPath.getPicpath());
            tempMap.put("author", commonStorage.getResult());
            tempMap.put("uptime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(res.getUptime()));
            tempMap.put("downloaded", res.getDownloaded());
//            将单条组装好的数据添加到分页主体数据容器中
            allData.add(tempMap);
        }
//        组装分页控制数据（前端控制分页的数据）
        pageResult.setData(allData);
        return pageResult;
    }

    public boolean strNoNull(String... strs) {
        boolean flag = true;
        for (String str : strs) {
            if (StringUtils.isBlank(str)) {
                flag = false;
            }
        }
        return flag;
    }

    public boolean strIsNumeric(String... strs) {
        boolean flag = true;
        for (String str : strs) {
            if (!StringUtils.isNumeric(str)) {
                flag = false;
            }
        }
        return flag;
    }

    public String getOrderKeyDB(String orderKey) {
        String orderKeyDB = "uptime";
        if (orderKey.equals("down")) {
            orderKeyDB = "downloaded";
        }
        return orderKeyDB;
    }

    public String getOrderStyleDB(String orderStyle) {
        String orderStyleDB = "DESC";
        if (orderStyle.equals("a")) {
            orderStyleDB = "ASC";
        }
        return orderStyleDB;
    }

    //    上传文件
    public String fileUpload(MultipartFile file) {
//        定义上传文件地址,这里我是以自己设置的Tomcat服务器的地址
        String basePath = env.getProperty("fileupload.baseurl");
//        获取上传的文件名,这里为了避免中文文件名上传失败，直接用UUID代替文件名了。
        String fileName = UUID.randomUUID().toString().replace("-", "");
        String originalFilename = file.getOriginalFilename();
//        文件后缀名
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String path = basePath + fileName + suffix;
//        使用第三方jar包jersey-core包和jersey-client包上传文件
//        创建客户端对象
        Client client = Client.create();
//        连接文件服务器
        WebResource resource = client.resource(path);
//        上传文件
        try {
            resource.put(file.getBytes());
        } catch (IOException e) {
            throw new MyException(ExceptionEnum.FILE_UPLOAD_FAILED);
        }
        return path;
    }
}
