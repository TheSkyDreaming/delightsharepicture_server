package cn.delightsharepicture.dao;

import cn.delightsharepicture.pojo.Path;
import tk.mybatis.mapper.common.Mapper;

public interface PathDao extends Mapper<Path> {
}
