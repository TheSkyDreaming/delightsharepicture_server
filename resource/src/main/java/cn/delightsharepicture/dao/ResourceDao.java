package cn.delightsharepicture.dao;

import cn.delightsharepicture.pojo.Resource;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface ResourceDao extends Mapper<Resource> {
    List<Resource> pageOrderFind();

    List<Resource> pageOrderClassifyFind(@Param("cid") int cid);

    List<Resource> containsIdFind(@Param("rids") List<Integer> rids, @Param("orderKey") String orderKey, @Param("orderStyle") String orderStyle);

    String findPsdPath(Integer rid);

    int addResDownQuantity(@Param("rid") int rid);

    List<Resource> pageOrderPersonalFind(@Param("uid") int uid);
}
