package cn.delightsharepicture.dao;

import cn.delightsharepicture.pojo.Classify;
import tk.mybatis.mapper.common.Mapper;

public interface ClassifyDao extends Mapper<Classify> {
}
