package cn.delightsharepicture.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class JwtInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
//        确认请求头中包含Authorization_DelightXiao这个头，这个可以自定义，由前后端商量好确定
        String header=request.getHeader("Authorization_DelightXiao");
        if(StringUtils.isNotBlank(header)){
//            确认请求头Authorization_DelightXiao的值以de_begin_str开头，这个可以自定义，由前后端商量好确定。
            if(header.startsWith("de_begin_str")){
                String token=header.substring(12);
                request.setAttribute("token",token);
            }
        }
        String uri=request.getRequestURI();
        StringBuffer url=request.getRequestURL();
        log.info("拦截到的uri:"+uri);
        log.info("拦截到的url:"+url.toString());
        return true;
    }
}
