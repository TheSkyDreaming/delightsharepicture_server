package cn.delightsharepicture.pojo;

import cn.delightsharepicture.entity.PageResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SearchStorage {
    private Integer statusCode;
    private PageResult result;
}
