package cn.delightsharepicture.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddResParam {
    private String resname;
    private Integer uid;
    private Integer cid;
    private MultipartFile picfile;
    private MultipartFile psdfile;
}
