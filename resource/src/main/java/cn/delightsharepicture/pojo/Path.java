package cn.delightsharepicture.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name="t_path")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Path implements Serializable {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer pid;
    private String picpath;
    private String psdpath;
}
