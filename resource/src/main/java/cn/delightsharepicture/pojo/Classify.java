package cn.delightsharepicture.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name="t_classify")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Classify implements Serializable {
    @Id
    @GeneratedValue(generator = "JDBC")
    Integer cid;
    String claname;
}
