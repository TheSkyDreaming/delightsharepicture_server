package cn.delightsharepicture.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "t_resource")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Resource implements Serializable {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer rid;
    private String resname;
    private Integer uid;
    private Integer cid;
    private Integer pid;
    private Date uptime;
    /*
     *     insertable的用于控制是否使用insert时传入的实体属性作为参数插入，
     *     因为数据库建库时有做默认值处理，所以这里设置为false.否则
     *     通用mapper会将入参实体类中的对应属性的null值覆盖掉数据库默认值。
     * */
    @Column(name = "downloaded", insertable = false)
    private Integer downloaded;
    private Path path;
}
