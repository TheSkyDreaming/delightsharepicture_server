package cn.delightsharepicture.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommonStorage {
    private Integer statusCode;
    private String result;
}
