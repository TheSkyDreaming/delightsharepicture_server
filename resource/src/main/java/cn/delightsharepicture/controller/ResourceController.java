package cn.delightsharepicture.controller;

import cn.delightsharepicture.entity.ResultEntity;
import cn.delightsharepicture.pojo.AddResParam;
import cn.delightsharepicture.service.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(description = "资源模块")
@RequestMapping("/resource")
public class ResourceController {

    @Autowired
    private ResourceService resService;

    @ApiOperation("获取全部主页资源")
    @GetMapping("/all/{orderKey}/{orderStyle}/{currPage}/{pageSize}")
    public ResponseEntity<ResultEntity> getAllHomeRes(@PathVariable("orderKey") String orderKey,@PathVariable("orderStyle") String orderStyle,@PathVariable("currPage") String currPage,@PathVariable("pageSize") String pageSize) {
        return ResponseEntity.ok(new ResultEntity(200, resService.getAllHomeResByPage(orderKey,orderStyle,currPage,pageSize)));
    }

    @ApiOperation("分类获取主页资源")
    @GetMapping("/classify/{classify}/{orderKey}/{orderStyle}/{currPage}/{pageSize}")
    public ResponseEntity<ResultEntity> getClassHomeRes(@PathVariable("classify") String classify,@PathVariable("orderKey") String orderKey,@PathVariable("orderStyle") String orderStyle,@PathVariable("currPage") String currPage,@PathVariable("pageSize") String pageSize){
        return ResponseEntity.ok(new ResultEntity(200, resService.getClassHomeResByPage(classify,orderKey,orderStyle,currPage,pageSize)));
    }

    @ApiOperation("主页搜索资源")
    @GetMapping("/search/{keyWord}/{orderKey}/{orderStyle}/{currPage}/{pageSize}")
    public ResponseEntity<ResultEntity> search(@PathVariable("keyWord") String keyWord,@PathVariable("orderKey") String orderKey,@PathVariable("orderStyle") String orderStyle,@PathVariable("currPage") String currPage,@PathVariable("pageSize") String pageSize){
        return ResponseEntity.ok(new ResultEntity(200,resService.search(keyWord,orderKey,orderStyle,currPage,pageSize)));
    }

    @ApiOperation("获取个人资源后台信息")
    @GetMapping("/personal/{uid}/{orderKey}/{orderStyle}/{currPage}/{pageSize}")
    public ResponseEntity<ResultEntity> getPersBackRes(@PathVariable("uid") String uid,@PathVariable("orderKey") String orderKey,@PathVariable("orderStyle") String orderStyle,@PathVariable("currPage") String currPage,@PathVariable("pageSize") String pageSize) {
        return ResponseEntity.ok(new ResultEntity(200, resService.getPersBackResByPage(uid,orderKey,orderStyle,currPage,pageSize)));
    }

    @ApiOperation("获取全部资源后台信息")
    @GetMapping("/manage/{orderKey}/{orderStyle}/{currPage}/{pageSize}")
    public ResponseEntity<ResultEntity> getAllBackRes(@PathVariable("orderKey") String orderKey,@PathVariable("orderStyle") String orderStyle,@PathVariable("currPage") String currPage,@PathVariable("pageSize") String pageSize) {
        return ResponseEntity.ok(new ResultEntity(200, resService.getAllBackResByPage(orderKey,orderStyle,currPage,pageSize)));
    }

    @ApiOperation("添加资源")
    @PostMapping("/")
    public ResponseEntity<ResultEntity> add(@ModelAttribute AddResParam param){
        return ResponseEntity.ok(new ResultEntity(200,resService.add(param)));
    }

    @ApiOperation("删除资源")
    @DeleteMapping("/{rid}")
    public ResponseEntity<ResultEntity> delete(@PathVariable("rid") String rid){
        return ResponseEntity.ok(new ResultEntity(200,resService.delete(rid)));
    }

    @ApiOperation("下载文件")
    @GetMapping("/down/{fileType}/{rid}")
    public ResponseEntity<ResultEntity> download(@PathVariable("fileType") String fileType,@PathVariable("rid") String rid){
        return ResponseEntity.ok(new ResultEntity(200,resService.download(fileType,rid)));
    }

}
