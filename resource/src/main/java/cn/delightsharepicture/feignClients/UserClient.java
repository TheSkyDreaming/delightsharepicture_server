package cn.delightsharepicture.feignClients;

import cn.delightsharepicture.feignClients.fallBack.UserClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@FeignClient(value = "user-service", fallback = UserClientFallBack.class)
public interface UserClient {
    @GetMapping("/userInfo/findNickName/{id}")
    String selectNickname(@PathVariable("id") String id);

    @PutMapping("/userInfo/addDownAmount/{uid}")
    String addDownNum(@PathVariable("uid") String uid);

    @PutMapping("/userInfo/addUploadAmount/{uid}")
    String addUploadNum(@PathVariable("uid") String uid);
}
