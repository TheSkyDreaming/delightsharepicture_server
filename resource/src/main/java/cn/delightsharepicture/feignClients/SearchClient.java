package cn.delightsharepicture.feignClients;

import cn.delightsharepicture.feignClients.fallBack.SearchClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "search-service",fallback = SearchClientFallBack.class)
public interface SearchClient {
    @GetMapping("/index/{key}/{orderKey}/{orderStyle}/{page}/{size}")
    String search(@PathVariable("key") String key, @PathVariable("orderKey") String orderKey, @PathVariable("orderStyle") String orderStyle, @PathVariable("page") int page, @PathVariable("size") int size);
}
