package cn.delightsharepicture.feignClients.fallBack;

import cn.delightsharepicture.feignClients.UserClient;
import org.springframework.stereotype.Component;

@Component
public class UserClientFallBack implements UserClient {
    @Override
    public String selectNickname(String id) {
        return "没有找到昵称哦！";
    }

    @Override
    public String addDownNum(String uid) {
        return "增加总下载量失败";
    }

    @Override
    public String addUploadNum(String uid) {
        return "增加上传资源量失败";
    }
}
