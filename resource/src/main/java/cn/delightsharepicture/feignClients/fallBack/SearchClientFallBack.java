package cn.delightsharepicture.feignClients.fallBack;

import cn.delightsharepicture.feignClients.SearchClient;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SearchClientFallBack implements SearchClient {
    @Override
    public String search(String key, String orderKey, String orderStyle, int page, int size) {
        return "服务器忙，请稍后再试。";
    }
}
