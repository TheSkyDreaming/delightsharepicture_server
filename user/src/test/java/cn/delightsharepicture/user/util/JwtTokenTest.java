package cn.delightsharepicture.user.util;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class JwtTokenTest {
    @Test
    public void CreateTokenTest(){
        JwtBuilder builder= Jwts.builder()
                .setId("2")
                .setSubject("user2")
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis()+600000))
                .signWith(SignatureAlgorithm.HS256,"delightxiao");
        log.debug("token value is:"+builder.compact());
    }
    @Test
    public void parseTokenTest(){
        Claims claims=Jwts.parser()
                .setSigningKey("delightxiao")
                .parseClaimsJws("eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIyIiwic3ViIjoidXNlcjIiLCJpYXQiOjE1ODQ4NzIyODMsImV4cCI6MTU4NDg3Mjg4M30.xuKfzqIVvTdvIpFVUNiHtfQvIkQew4uAwPdtESxfU34")
                .getBody();
        log.debug("userid:"+claims.getId());
        log.debug("username:"+claims.getSubject());
        log.debug("regtime:"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(claims.getIssuedAt()));
        log.debug("expiretime:"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(claims.getExpiration()));
    }
    @Test
    public void timete(){
        log.info("dategettime:"+new Date().getTime());
        log.info("currentmillis:"+System.currentTimeMillis());
    }
}
