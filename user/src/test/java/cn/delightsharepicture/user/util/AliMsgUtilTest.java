package cn.delightsharepicture.user.util;

import cn.delightsharepicture.pojo.AliMsgRespEntity;
import cn.delightsharepicture.utils.AliMsgUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.IOException;


@Slf4j
public class AliMsgUtilTest {

    @Test
    public void msgTest(){
        String msg = new AliMsgUtil().sendMsg("01234567890","123456","login");
        log.info("阿里短信返回的信息："+msg);
    }
    @Test
    public void jsonToObj(){
        String jsonStr="{\"Message\":\"OK\",\"RequestId\":\"7BDF93F4-DA31-4F74-B46D-CEA36181C2E2\",\"BizId\":\"547320387554035274^0\",\"Code\":\"OK\"}";
        try {
            AliMsgRespEntity entiy = new ObjectMapper().readValue(jsonStr, AliMsgRespEntity.class);
            log.info(entiy.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }
}
