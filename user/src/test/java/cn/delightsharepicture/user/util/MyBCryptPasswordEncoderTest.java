package cn.delightsharepicture.user.util;


import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Slf4j
public class MyBCryptPasswordEncoderTest {
    private BCryptPasswordEncoder encoder;

    @Before
    public void bef() {
        encoder= new BCryptPasswordEncoder();
    }

    @Test
    public void encodeTest() {
        log.info("user:" + encoder.encode("user"));
        log.info("user2:" + encoder.encode("user2"));
        log.info("user3:" + encoder.encode("user3"));
        log.info("user4:" + encoder.encode("user4"));
        log.info("admin:" + encoder.encode("admin"));
    }

    @Test
    public void matcherTest() {
        boolean usf = encoder.matches("user", "$2a$10$43cTQMy9BMmKvBIJM79IfOzt4ZCRIIe/zQKGOzC3h9/KWSES.zQgG");
        if (usf){
            log.info("match success");
        }else {
            log.info("match fail");
        }
    }
}
