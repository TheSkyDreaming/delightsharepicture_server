package cn.delightsharepicture.controller;

import cn.delightsharepicture.entity.ResultEntity;
import cn.delightsharepicture.pojo.Admin;
import cn.delightsharepicture.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(description = "管理员模块")
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @ApiOperation("管理员登录")
    @PostMapping("/login")
    public ResponseEntity<ResultEntity> adminLogin(@RequestBody Admin admin) {
        return ResponseEntity.ok(new ResultEntity(200,adminService.login(admin)));
    }
}
