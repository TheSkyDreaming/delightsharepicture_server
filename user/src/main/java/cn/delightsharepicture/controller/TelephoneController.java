package cn.delightsharepicture.controller;

import cn.delightsharepicture.entity.ResultEntity;
import cn.delightsharepicture.pojo.RegTelParam;
import cn.delightsharepicture.pojo.Telephone;
import cn.delightsharepicture.service.TelephoneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(description="手机号业务模块")
@RequestMapping("/telephone")
public class TelephoneController {

    @Autowired
    private TelephoneService telService;

    @ApiOperation("手机号登录")
    @PostMapping("/login")
    public ResponseEntity<ResultEntity> loginByTel(@RequestBody Telephone telephone){
        return ResponseEntity.ok(new ResultEntity(200, telService.loginByTel(telephone)));
    }
    @ApiOperation("手机号注册")
    @PostMapping("/register")
    public ResponseEntity<ResultEntity> registerByTel(@RequestBody RegTelParam param){
        return ResponseEntity.ok(new ResultEntity(200, telService.registerTel(param)));
    }
    @ApiOperation("发送验证码")
    @PostMapping("/sendMsg/{opeType}")
    public ResponseEntity<ResultEntity> sendMsg(@RequestBody Telephone telephone,@PathVariable("opeType") String opeType){
        return ResponseEntity.ok(new ResultEntity(200, telService.sendMsg(telephone,opeType)));
    }

}
