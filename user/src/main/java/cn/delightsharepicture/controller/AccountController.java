package cn.delightsharepicture.controller;


import cn.delightsharepicture.entity.ResultEntity;
import cn.delightsharepicture.pojo.Account;
import cn.delightsharepicture.pojo.RegAccParam;
import cn.delightsharepicture.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(description = "账号模块")
@RestController
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private AccountService accService;

    @ApiOperation("用户名密码登录")
    @PostMapping("/login")
    public ResponseEntity<ResultEntity> loginByUp(@RequestBody Account account){
        return ResponseEntity.ok(new ResultEntity(200, accService.loginByUP(account)));
    }
    @ApiOperation("用户名密码注册")
    @PostMapping("/register")
    public ResponseEntity<ResultEntity> registerByUp(@RequestBody RegAccParam param){
        return ResponseEntity.ok(new ResultEntity(200, accService.register(param)));
    }

}
