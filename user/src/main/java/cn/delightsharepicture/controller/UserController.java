package cn.delightsharepicture.controller;

import cn.delightsharepicture.entity.ResultEntity;
import cn.delightsharepicture.pojo.ModAccParam;
import cn.delightsharepicture.pojo.User;
import cn.delightsharepicture.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "用户模块")
@RestController
@RequestMapping("/userInfo")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation("查询所有账号用户")
    @GetMapping("/findAllAcc/{orderKey}/{orderStyle}/{currPage}/{pageSize}")
    public ResponseEntity<ResultEntity> findAllAcc(@PathVariable("orderKey") String orderKey,@PathVariable("orderStyle") String orderStyle,@PathVariable("currPage") String currPage,@PathVariable("pageSize") String pageSize){
        return ResponseEntity.ok(new ResultEntity(200, userService.findAllAcc(orderKey,orderStyle,currPage,pageSize)));
    }

    @ApiOperation("查询所有手机用户")
    @GetMapping("/findAllTel/{orderKey}/{orderStyle}/{currPage}/{pageSize}")
    public ResponseEntity<ResultEntity> findAllTel(@PathVariable("orderKey") String orderKey,@PathVariable("orderStyle") String orderStyle,@PathVariable("currPage") String currPage,@PathVariable("pageSize") String pageSize){
        return ResponseEntity.ok(new ResultEntity(200, userService.findAllTel(orderKey,orderStyle,currPage,pageSize)));
    }

    @ApiOperation("查询昵称")
    @GetMapping("/findNickName/{id}")
    public ResponseEntity<ResultEntity> findNickName(@PathVariable("id") String id) {
        return ResponseEntity.ok(new ResultEntity(200, userService.findNickName(id)));
    }

    @ApiOperation("删除账户")
    @DeleteMapping("/{uid}")
    public ResponseEntity<ResultEntity> deleteAccount(@PathVariable("uid") String uid) {
        return ResponseEntity.ok(new ResultEntity(200, userService.delete(uid)));
    }

    @ApiOperation("修改账号用户信息")
    @PutMapping("/acc")
    public ResponseEntity<ResultEntity> modifyAccount(@RequestBody ModAccParam param) {
        return ResponseEntity.ok(new ResultEntity(200, userService.accModify(param)));
    }

    @ApiOperation("修改手机用户信息")
    @PutMapping("/tel")
    public ResponseEntity<ResultEntity> modifyTel(@RequestBody User user) {
        return ResponseEntity.ok(new ResultEntity(200, userService.telModify(user)));
    }

    @ApiOperation("增加用户总下载量")
    @PutMapping("/addDownAmount/{uid}")
    public ResponseEntity<ResultEntity> addDownAmount(@PathVariable("uid") String uid){
        return ResponseEntity.ok(new ResultEntity(200,userService.addDownAmount(uid)));
    }

    @ApiOperation("增加上传资源数量")
    @PutMapping("/addUploadAmount/{uid}")
    public ResponseEntity<ResultEntity> addUploadAmount(@PathVariable("uid") String uid){
        return ResponseEntity.ok(new ResultEntity(200,userService.addUploadAmount(uid)));
    }

    @ApiOperation("查询当前用户信息")
    @GetMapping("/me/{uid}")
    public ResponseEntity<ResultEntity> getMineInfo(@PathVariable("uid") String uid){
        return ResponseEntity.ok(new ResultEntity(200,userService.getMineInfo(uid)));
    }

}
