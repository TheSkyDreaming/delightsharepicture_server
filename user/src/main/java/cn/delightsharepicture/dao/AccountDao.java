package cn.delightsharepicture.dao;

import cn.delightsharepicture.pojo.Account;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface AccountDao extends Mapper<Account> {

    int modify(@Param("uid") Integer uid, @Param("password") String password);

    List<Account> pageOrderFind();
}
