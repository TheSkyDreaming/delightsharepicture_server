package cn.delightsharepicture.dao;


import cn.delightsharepicture.pojo.User;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface UserDao extends Mapper<User> {
    int addDownAmount(@Param("uid")int uid);

    int addUploadAmount(@Param("uid")int uid);
}
