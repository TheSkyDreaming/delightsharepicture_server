package cn.delightsharepicture.dao;

import cn.delightsharepicture.pojo.Admin;
import tk.mybatis.mapper.common.Mapper;

public interface AdminDao extends Mapper<Admin> {
}
