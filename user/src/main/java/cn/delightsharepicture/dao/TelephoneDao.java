package cn.delightsharepicture.dao;

import cn.delightsharepicture.pojo.Account;
import cn.delightsharepicture.pojo.Telephone;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TelephoneDao extends Mapper<Telephone> {
    List<Telephone> pageOrderFind();
}
