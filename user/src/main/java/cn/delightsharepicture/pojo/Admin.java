package cn.delightsharepicture.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="t_admin")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Admin  implements Serializable {
    @Id
    private Integer aid;
    private String adacc;
    private String adpas;
}
