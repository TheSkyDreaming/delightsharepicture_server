package cn.delightsharepicture.pojo;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class AliMsgRespEntity {

//    @JsonAlias注解用于指定json字符串中的key，可以应用于解决
//    第三方接口返回json数据格式key值首字母是大写导致无法json转换成对象的问题。
    @JsonAlias("Message")
    private String message;
    @JsonAlias("RequestId")
    private String requestId;
    @JsonAlias("BizId")
    private String bizId;
    @JsonAlias("Code")
    private String code;

    @Override
    public String toString() {
        return "AliMsgRespEntiy{" +
                "message='" + message + '\'' +
                ", requestId='" + requestId + '\'' +
                ", bizId='" + bizId + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
