package cn.delightsharepicture.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="t_account")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {
    @Id
    private String username;
    private String password;
    /*
    * 注意，即使下面已经定义了和account有关的user实体类，这个uid依然要写，
    * 因为在插入数据的时候需要用到这个属性。
    * */
    private Integer uid;
    private User user;
}
