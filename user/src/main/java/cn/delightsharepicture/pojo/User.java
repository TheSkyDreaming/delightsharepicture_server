package cn.delightsharepicture.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "t_user")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    @Id
//    使用tkmapper时使用mysql数据库自增长主键
    @GeneratedValue(generator = "JDBC")
    private Integer uid;
    private String nickname;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birth;
    /*
     *     insertable的用于控制是否使用insert时传入的实体属性作为参数插入，
     *     因为数据库建库时有做默认值处理，所以这里设置为false.否则
     *     通用mapper会将入参实体类中的对应属性的null值覆盖掉数据库默认值。
     * */
    @Column(name = "gender", insertable = false)
    private Integer gender;
    @Column(name = "downloaded", insertable = false)
    private Integer downloaded;
    @Column(name = "upload", insertable = false)
    private Integer upload;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date regtime;
}
