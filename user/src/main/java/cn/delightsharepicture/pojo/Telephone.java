package cn.delightsharepicture.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="t_telephone")
public class Telephone implements Serializable {
    @Id
    private String tel;
    private Integer uid;
    private User user;
    @Transient
//    @JsonIgnore
    private String checkCode;
}
