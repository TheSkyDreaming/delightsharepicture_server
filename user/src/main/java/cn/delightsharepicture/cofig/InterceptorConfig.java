package cn.delightsharepicture.cofig;

import cn.delightsharepicture.interceptor.JwtInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private JwtInterceptor interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/account/login/**")
                .excludePathPatterns("/telphone/login/**")
                .excludePathPatterns("/telphone/register/**")
                .excludePathPatterns("/account/register");
//        如果有多个拦截器就再写一个registry.addInterceptor
    }
}
