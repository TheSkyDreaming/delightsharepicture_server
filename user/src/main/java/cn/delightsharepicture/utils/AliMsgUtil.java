package cn.delightsharepicture.utils;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.stereotype.Component;

@Component
public class AliMsgUtil {
    public String sendMsg(String telephone,String checkCode, String msgType) {

        String templateCode;
        if (msgType.equals("login")) {
            templateCode = "SMS_188640515";
        } else {
            templateCode = "SMS_188625503";
        }

        //        这个accessKeyId和secret的值在每次提交前都要重置成非正确的值。否则会有安全问题。
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4G2RqnPGCzySxn8d2Z1Z", "RpsYfs3SJG7ZA5RMNaCyyKHr3gHM6E");
//        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "I don't know", "I don't know");

        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", telephone);
        request.putQueryParameter("SignName", "悦图网的shortMsg");
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + checkCode + "\"}");

        String responseInfo = "";
        try {
            CommonResponse response = client.getCommonResponse(request);
            responseInfo = response.getData();
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (
                ClientException e) {
            e.printStackTrace();
        }
        return responseInfo;
    }
}
