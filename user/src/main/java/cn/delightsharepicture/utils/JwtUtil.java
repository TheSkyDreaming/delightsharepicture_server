package cn.delightsharepicture.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@Component
@ConfigurationProperties("jwt.config")
public class JwtUtil {

    private String key ;

    private long expirationTime;//一个小时

    /**
     * 生成JWT
     *
     * @param id
     * @param subject
     * @return
     */
    public  String createJWT(String id, String subject, String roles) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        JwtBuilder builder = Jwts.builder().setId(id)
                .setSubject(subject)
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS256, key)
                .claim("roles", roles)
                .claim("loginFlag",true);
        if (expirationTime > 0) {
            builder.setExpiration( new Date( nowMillis + expirationTime));
        }
        return builder.compact();
    }

    /**
     * 解析JWT
     * @param jwtStr
     * @return
     */
    public Claims parseJWT(String jwtStr){
//        返回的claims对象可以
//        获取id：claims.getId()
//        获取目标用户：claims.getSubject()
//        获取token签发时间：claims.getIssuedAt()
//        获取token过期时间：claims.getExpiration()
        return  Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(jwtStr)
                .getBody();
    }

}
