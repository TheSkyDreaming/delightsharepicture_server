package cn.delightsharepicture.service;

import cn.delightsharepicture.pojo.Admin;

import java.util.Map;

public interface AdminService {
    Map<String,Object> login(Admin admin);
}
