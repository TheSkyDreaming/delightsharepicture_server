package cn.delightsharepicture.service;

import cn.delightsharepicture.pojo.RegTelParam;
import cn.delightsharepicture.pojo.Telephone;
import cn.delightsharepicture.pojo.User;

import java.util.Map;

public interface TelephoneService {
    Map<String, Object> loginByTel(Telephone telephone);

    String registerTel(RegTelParam param);

    String sendMsg(Telephone telephone, String opeType);

}
