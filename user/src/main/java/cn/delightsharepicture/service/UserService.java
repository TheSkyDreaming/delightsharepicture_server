package cn.delightsharepicture.service;

import cn.delightsharepicture.entity.PageResult;
import cn.delightsharepicture.pojo.ModAccParam;
import cn.delightsharepicture.pojo.User;


public interface UserService {

    String findNickName(String id);

    String delete(String uid);

    String accModify(ModAccParam param);

    String telModify(User user);

    PageResult findAllAcc(String orderKey, String orderStyle, String currPage, String pageSize);

    PageResult findAllTel(String orderKey, String orderStyle, String currPage, String pageSize);

    String addDownAmount(String uid);

    String addUploadAmount(String uid);

    User getMineInfo(String uid);
}
