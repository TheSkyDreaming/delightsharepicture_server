package cn.delightsharepicture.service.impl;

import cn.delightsharepicture.dao.TelephoneDao;
import cn.delightsharepicture.dao.UserDao;
import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import cn.delightsharepicture.exceptionhandle.exception.MyException;
import cn.delightsharepicture.pojo.AliMsgRespEntity;
import cn.delightsharepicture.pojo.RegTelParam;
import cn.delightsharepicture.pojo.Telephone;
import cn.delightsharepicture.pojo.User;
import cn.delightsharepicture.service.TelephoneService;
import cn.delightsharepicture.utils.AliMsgUtil;
import cn.delightsharepicture.utils.JwtUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@Slf4j
@Service
public class TelephoneServiceImpl implements TelephoneService {

    @Autowired
    private TelephoneDao telDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private AliMsgUtil aliMsgUtil;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private ObjectMapper objMap;
    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public Map<String, Object> loginByTel(Telephone telephone) {
        boolean infoIntegrity = false;
        String tel = "";
        String checkCode = "";
        if (telephone != null) {
            tel = telephone.getTel();
            checkCode = telephone.getCheckCode();
            if (strNoNull(tel, checkCode)) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        Telephone phone = telDao.selectByPrimaryKey(tel);
        if (phone == null) {
            throw new MyException(ExceptionEnum.USER_NOT_EXIST);
        }
        String redisCode = (String) redisTemplate.opsForValue().get("checkCode_login_" + tel);
        if (StringUtils.isBlank(redisCode) || !checkCode.equals(redisCode)) {
            throw new MyException(ExceptionEnum.CHECKCODE_ERROR);
        }
        String token = jwtUtil.createJWT(phone.getUid().toString(), tel, "common_user");
        Integer uid = phone.getUid();
        Map<String, Object> result = new HashMap<>();
        result.put("token", token);
        result.put("uid", uid);
        result.put("role", "comm_user");
        return result;
    }

    //    记得加权限控制，还没做
    @Override
    @Transactional
    public String registerTel(RegTelParam param) {
        boolean infoIntegrity = false;
        String tel = "";
        String checkcode = "";
        String nickname = "";
        if (param != null) {
            tel = param.getTel();
            checkcode = param.getCheckcode();
            nickname = param.getNickname();
            if (strNoNull(tel, nickname, checkcode)) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        Telephone phone = telDao.selectByPrimaryKey(tel);
        if (phone != null) {
            throw new MyException(ExceptionEnum.USER_EXISTED);
        }
        String redisCode = (String) redisTemplate.opsForValue().get("checkCode_register_" + tel);
        if (StringUtils.isBlank(redisCode)) {
            throw new MyException(ExceptionEnum.CHECKCODE_ERROR);
        }
        if (!checkcode.equals(redisCode)) {
            throw new MyException(ExceptionEnum.CHECKCODE_ERROR);
        }
        User user = new User();
        user.setRegtime(new Timestamp(System.currentTimeMillis()));
        user.setNickname(nickname);
        int userNum = userDao.insert(user);
        if (userNum > 0) {
            Telephone telephone = new Telephone();
            telephone.setTel(tel);
            telephone.setUid(user.getUid());
            int telNum = telDao.insert(telephone);
            if (telNum > 0) {
                return "注册成功";
            } else {
                throw new MyException(ExceptionEnum.OPERATION_FAILED);
            }
        } else {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
    }

    @Override
    public String sendMsg(Telephone telephone, String opeType) {
        boolean infoIntegrity = false;
        String tel = "";
        if (telephone != null) {
            tel = telephone.getTel();
            if (StringUtils.isNotBlank(tel) && StringUtils.isNotBlank(opeType)) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        String code = String.valueOf((int) (100000 + Math.random() * 899999));
        redisTemplate.opsForValue().set("checkCode_" + opeType + "_" + tel, code, 120, TimeUnit.MINUTES);
//        log.info("生成的验证码为："+code);
//        return "验证码发送成功";
        String responseMsg = aliMsgUtil.sendMsg(tel, code, opeType);
        AliMsgRespEntity entity = null;
        try {
            entity = objMap.readValue(responseMsg, AliMsgRespEntity.class);
        } catch (IOException e) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        if ("OK".equals(entity.getMessage()) && "OK".equals(entity.getCode())) {
            return "信息发送成功";
        } else {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
    }

    public boolean strNoNull(String... strs) {
        boolean flag = true;
        for (String str : strs) {
            if (StringUtils.isBlank(str)) {
                flag = false;
            }
        }
        return flag;
    }
}
