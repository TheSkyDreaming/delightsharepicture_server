package cn.delightsharepicture.service.impl;

import cn.delightsharepicture.dao.AccountDao;
import cn.delightsharepicture.dao.UserDao;
import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import cn.delightsharepicture.exceptionhandle.exception.MyException;
import cn.delightsharepicture.pojo.Account;
import cn.delightsharepicture.pojo.ModAccParam;
import cn.delightsharepicture.pojo.RegAccParam;
import cn.delightsharepicture.pojo.User;
import cn.delightsharepicture.service.AccountService;
import cn.delightsharepicture.utils.JwtUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public Map<String, Object> loginByUP(Account account) {
        boolean infoIntegrity = false;
        String username = "";
        String password = "";
        if (account != null) {
            username = account.getUsername();
            password = account.getPassword();
            if (strNoNull(username, password)) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        Account acc = accountDao.selectByPrimaryKey(username);
        if (acc == null) {
            throw new MyException(ExceptionEnum.USER_NOT_EXIST);
        }
//        密码不匹配
        if (!encoder.matches(password, acc.getPassword())) {
            throw new MyException(ExceptionEnum.PASS_ERROR);
        }
        String token = jwtUtil.createJWT(acc.getUid().toString(), acc.getUsername(), "comm_user");
        Integer uid = acc.getUid();
        Map<String, Object> result = new HashMap<>();
        result.put("token", token);
        result.put("uid", uid);
        result.put("role", "comm_user");
        return result;
    }

    @Override
    @Transactional
    public String register(RegAccParam param) {
        boolean infoIntegrity = false;
        String username = "";
        String password = "";
        String nickname = "";
        if (param != null) {
            username = param.getUsername();
            password = param.getPassword();
            nickname = param.getNickname();
            if (strNoNull(username, password, nickname)) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        Account acc = accountDao.selectByPrimaryKey(username);
        if (acc != null) {
            throw new MyException(ExceptionEnum.USER_EXISTED);
        }
        User user = new User();
        user.setRegtime(new Date());
        user.setNickname(nickname);
        int userNum = userDao.insert(user);
        if (userNum != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        Account account = new Account();
        account.setUsername(username);
        account.setUid(user.getUid());
        account.setPassword(encoder.encode(password));
        int accNum = accountDao.insert(account);
        if (accNum != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        return "注册成功";
    }

    public boolean strNoNull(String... strs) {
        boolean flag = true;
        for (String str : strs) {
            if (StringUtils.isBlank(str)) {
                flag = false;
            }
        }
        return flag;
    }
}
