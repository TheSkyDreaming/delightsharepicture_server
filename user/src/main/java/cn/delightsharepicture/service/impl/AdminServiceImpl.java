package cn.delightsharepicture.service.impl;

import cn.delightsharepicture.dao.AdminDao;
import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import cn.delightsharepicture.exceptionhandle.exception.MyException;
import cn.delightsharepicture.pojo.Admin;
import cn.delightsharepicture.service.AdminService;
import cn.delightsharepicture.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class AdminServiceImpl implements AdminService {

    @Autowired
    BCryptPasswordEncoder encoder;
    @Autowired
    AdminDao adminDao;
    @Autowired
    JwtUtil jwtUtil;

    @Override
    public Map<String, Object> login(Admin admin) {
        boolean infoIntegrity = false;
        String adacc = "";
        String adpas = "";
        if (null != admin) {
            adacc = admin.getAdacc();
            adpas = admin.getAdpas();
            if (StringUtils.isNotBlank(adacc) && StringUtils.isNotBlank(adpas)) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        Admin queryAdmin = new Admin();
        queryAdmin.setAdacc(adacc);
        Admin resAdmin = adminDao.selectOne(queryAdmin);
        if (null == resAdmin) {
            throw new MyException(ExceptionEnum.USER_NOT_EXIST);
        }
        if (!encoder.matches(adpas, resAdmin.getAdpas())) {
            throw new MyException(ExceptionEnum.PASS_ERROR);
        }
        String token = jwtUtil.createJWT(resAdmin.getAid().toString(), adacc, "admin_user");
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("token", token);
        retMap.put("role", "admin_user");
        return retMap;
    }
}
