package cn.delightsharepicture.service.impl;

import cn.delightsharepicture.dao.AccountDao;
import cn.delightsharepicture.dao.TelephoneDao;
import cn.delightsharepicture.dao.UserDao;
import cn.delightsharepicture.entity.PageResult;
import cn.delightsharepicture.exceptionhandle.ExceptionEnum;
import cn.delightsharepicture.exceptionhandle.exception.MyException;
import cn.delightsharepicture.pojo.Account;
import cn.delightsharepicture.pojo.ModAccParam;
import cn.delightsharepicture.pojo.Telephone;
import cn.delightsharepicture.pojo.User;
import cn.delightsharepicture.service.UserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private AccountDao accDao;
    @Autowired
    private TelephoneDao telDao;
    @Autowired
    private BCryptPasswordEncoder encoder;


    @Override
    public String findNickName(String id) {
        if (StringUtils.isBlank(id)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        return userDao.selectByPrimaryKey(id).getNickname();
    }

    //    记得加权限控制，还没做
    @Override
    @Transactional
    public String delete(String uid) {
        if (StringUtils.isBlank(uid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!StringUtils.isNumeric(uid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        int delNum = userDao.deleteByPrimaryKey(Integer.parseInt(uid));
        if (delNum != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        return "删除成功";
    }

    //    记得加权限控制，还没做
    @Override
    @Transactional
    public String accModify(ModAccParam param) {
        boolean infoIntegrity = false;
        Integer uid = -1;
        String password = "";
        String nickname = "";
        Date birth = null;
        Integer gender = -1;
        if (param != null) {
            uid = param.getUid();
            password = param.getPassword();
            nickname = param.getNickname();
            birth = param.getBirth();
            gender = param.getGender();
            if (strNoNull(password, nickname) && null != birth && uid > 0 && (gender == 0 || gender == 1)) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        User user = userDao.selectByPrimaryKey(uid);
        if (null == user) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        int modAccNum = accDao.modify(uid, encoder.encode(password));
        if (modAccNum != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        user.setNickname(nickname);
        user.setBirth(birth);
        user.setGender(gender);
        int modUserNum = userDao.updateByPrimaryKey(user);
        if (modUserNum != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        return "修改成功";
    }

    //    记得加权限控制，还没做
    @Override
    @Transactional
    public String telModify(User user) {
        boolean infoIntegrity = false;
        Integer uid = -1;
        String nickname = "";
        Date birth = null;
        Integer gender = -1;
        if (user != null) {
            uid = user.getUid();
            nickname = user.getNickname();
            birth = user.getBirth();
            gender = user.getGender();
            if (StringUtils.isNotBlank(nickname) && null != birth && uid > 0 && (gender == 0 || gender == 1)) {
                infoIntegrity = true;
            }
        }
        if (!infoIntegrity) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        User userDb = userDao.selectByPrimaryKey(uid);
        if (null == user) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        userDb.setNickname(nickname);
        userDb.setBirth(birth);
        userDb.setGender(gender);
        int modNum = userDao.updateByPrimaryKey(userDb);
        if (modNum != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        return "修改成功";
    }

    @Override
    public PageResult findAllAcc(String orderKey, String orderStyle, String currPage, String pageSize) {
        if (!strNoNull(orderKey, orderStyle, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!strIsNumeric(currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
//        pageheler可以排序，使用方法为设置startPage函数的第三个参数为一个Stirng字符串，内容为排序字段+空格+排序方式
//        比如要根据字段score倒序查询，则该字符串的内容为："score desc".
        Page page = PageHelper.startPage(Integer.parseInt(currPage), Integer.parseInt(pageSize), getOrderKeyDB(orderKey) + " " + getOrderStyleDB(orderStyle));
        List<Account> accs = accDao.pageOrderFind();
        if (accs == null || accs.size() == 0) {
            throw new MyException(ExceptionEnum.USER_NOT_EXIST);
        }
        List<Map<String,Object>> allData = new ArrayList<>();
        Map<String,Object> temMap;
        Iterator<Account> iterator = accs.iterator();
        while(iterator.hasNext()){
            Account temAcc = iterator.next();
            temMap = new HashMap<>();
            temMap.put("username",temAcc.getUsername());
            temMap.put("uid",temAcc.getUser().getUid());
            temMap.put("nickname",temAcc.getUser().getNickname());
            temMap.put("regtime",temAcc.getUser().getRegtime());
            temMap.put("upload",temAcc.getUser().getUpload());
            temMap.put("downloaded",temAcc.getUser().getDownloaded());
            allData.add(temMap);
        }
        PageResult result = new PageResult();
        result.setCurrPage(page.getPageNum());
        result.setPageSize(page.getPageSize());
        result.setTotalPages(page.getPages());
        result.setTotalSize(page.getTotal());
        result.setData(allData);
        return result;
    }

    @Override
    public PageResult findAllTel(String orderKey, String orderStyle, String currPage, String pageSize) {
        if (!strNoNull(orderKey, orderStyle, currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!strIsNumeric(currPage, pageSize)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
//        pageheler可以排序，使用方法为设置startPage函数的第三个参数为一个Stirng字符串，内容为排序字段+空格+排序方式
//        比如要根据字段score倒序查询，则该字符串的内容为："score desc".
        Page page = PageHelper.startPage(Integer.parseInt(currPage), Integer.parseInt(pageSize), getOrderKeyDB(orderKey) + " " + getOrderStyleDB(orderStyle));
        List<Telephone> tels = telDao.pageOrderFind();
        if (tels == null || tels.size() == 0) {
            throw new MyException(ExceptionEnum.USER_NOT_EXIST);
        }
        List<Map<String,Object>> allData = new ArrayList<>();
        Map<String,Object> temMap;
        Iterator<Telephone> iterator = tels.iterator();
        while(iterator.hasNext()){
            Telephone temTel = iterator.next();
            temMap = new HashMap<>();
            temMap.put("tel",temTel.getTel());
            temMap.put("uid",temTel.getUser().getUid());
            temMap.put("nickname",temTel.getUser().getNickname());
            temMap.put("regtime",temTel.getUser().getRegtime());
            temMap.put("upload",temTel.getUser().getUpload());
            temMap.put("downloaded",temTel.getUser().getDownloaded());
            allData.add(temMap);
        }
        PageResult result = new PageResult();
        result.setCurrPage(page.getPageNum());
        result.setPageSize(page.getPageSize());
        result.setTotalPages(page.getPages());
        result.setTotalSize(page.getTotal());
        result.setData(allData);
        return result;
    }

    @Override
    public String addDownAmount(String uid) {
        if (StringUtils.isBlank(uid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!StringUtils.isNumeric(uid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        int affectRow = userDao.addDownAmount(Integer.parseInt(uid));
        if (affectRow != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        return "增加总下载量成功";
    }

    @Override
    public String addUploadAmount(String uid) {
        if (StringUtils.isBlank(uid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!StringUtils.isNumeric(uid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        int affectRow = userDao.addUploadAmount(Integer.parseInt(uid));
        if (affectRow != 1) {
            throw new MyException(ExceptionEnum.OPERATION_FAILED);
        }
        return "增加上传资源数量成功";
    }

    @Override
    public User getMineInfo(String uid) {
        if (StringUtils.isBlank(uid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_INTEGRITY);
        }
        if (!StringUtils.isNumeric(uid)) {
            throw new MyException(ExceptionEnum.PARAM_NOT_DIGIT);
        }
        User user = userDao.selectByPrimaryKey(Integer.parseInt(uid));
        return user;
    }

    public boolean strNoNull(String... strs) {
        boolean flag = true;
        for (String str : strs) {
            if (StringUtils.isBlank(str)) {
                flag = false;
            }
        }
        return flag;
    }

    public String getOrderKeyDB(String orderKey) {
        String orderKeyDB = "regtime";
        if (orderKey.equals("down")) {
            orderKeyDB = "downloaded";
        }
        if (orderKey.equals("uplo")) {
            orderKeyDB = "upload";
        }
        if (orderKey.equals("nick")) {
            orderKeyDB = "nickname";
        }
        return orderKeyDB;
    }

    public String getOrderStyleDB(String orderStyle) {
        String orderStyleDB = "DESC";
        if (orderStyle.equals("a")) {
            orderStyleDB = "ASC";
        }
        return orderStyleDB;
    }

    public boolean strIsNumeric(String... strs) {
        boolean flag = true;
        for (String str : strs) {
            if (!StringUtils.isNumeric(str)) {
                flag = false;
            }
        }
        return flag;
    }
}
