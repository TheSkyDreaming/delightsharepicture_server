package cn.delightsharepicture.service;

import cn.delightsharepicture.pojo.Account;
import cn.delightsharepicture.pojo.ModAccParam;
import cn.delightsharepicture.pojo.RegAccParam;

import java.util.List;
import java.util.Map;

public interface AccountService {

    Map<String, Object> loginByUP(Account account);

    String register(RegAccParam param);


}
